﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float movementSpeed = 3.0f;
    private Vector2 movement = new Vector2();
    private Rigidbody2D rigidBody;

    private Animator animator;
    private string animationState = "AnimationState";
    enum AnimStates
    {
        idle = 0,
        walk_down = 1,
        walk_right = 2,
        walk_up = 3,
        walk_left = 4
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        MovementHandler();
        AnimHandler();
    }

    private void MovementHandler()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        rigidBody.velocity = movement * movementSpeed;
    }

    private void AnimHandler()
    {
        if (movement.x > 0)
        {
            animator.SetInteger(animationState, (int)AnimStates.walk_right);
        }
        else if (movement.x < 0)
        {
            animator.SetInteger(animationState, (int)AnimStates.walk_left);
        }
        else if (movement.y > 0)
        {
            animator.SetInteger(animationState, (int)AnimStates.walk_up);
        }
        else if (movement.y < 0)
        {
            animator.SetInteger(animationState, (int)AnimStates.walk_down);
        }
        else
        {
            animator.SetInteger(animationState, (int)AnimStates.idle);
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Kolizija s " + collision.tag);
        if (collision.tag == "Coin")
        {
            Destroy(collision.gameObject);
        }
        else if (collision.tag == "Heart")
        {
            Destroy(collision.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Ender: " + collision.gameObject.tag);
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        Debug.Log("Stay: " + collision.gameObject.tag);
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log("Exit: " + collision.gameObject.tag);
    }
}